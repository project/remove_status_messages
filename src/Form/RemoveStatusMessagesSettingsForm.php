<?php

namespace Drupal\remove_status_messages\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure the remove_status_messages module settings.
 *
 * @package Drupal\remove_status_messages\Form
 */
class RemoveStatusMessagesSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'remove_status_messages_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['remove_status_messages.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('remove_status_messages.settings');

    $form['url_patterns'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Page URLs patterns'),
      '#default_value' => $config->get('url_patterns'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $config = $this->config('remove_status_messages.settings');

    $config
      ->set('url_patterns', $form_state->getValue('url_patterns'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
