# Remove Status Messages

This module removes the status messages from a specific set of page URLs.

There is, by default, no way to totally remove status messages from a page.

Even if the Messages block is disabled or removed from the page theme, Drupal
inject the status messages in the page via the `BlockPageVariant` `build`
method:

```php
    // If no block displays status messages, still render them.
    if (!$messages_block_displayed) {
      $build['content']['messages'] = [
        '#weight' => -1000,
        '#type' => 'status_messages',
        '#include_fallback' => TRUE,
      ];
    }
```

This module implements a `hook_preprocess_page` which removes the `messages`
render array for a specific set of page URLs.

**Use case**: do not display status messages on a page triggering an immediate
redirect to rather display them after the redirection has occurred.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/remove_status_messages).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/remove_status_messages).


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module.
For further information, see [Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. Enable the module at Administration > Extend.
2. Go to `/admin/config/system/remove_status_messages/settings` to configure
the URL patterns of the pages you want to remove the status messages from.


## Maintainers

- Frank Mably - [mably](https://www.drupal.org/u/mably)
